package be.multimedi.pokemonDB.trainer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TrainerTest {
   @Test
   public void createTrainerTest(){
      Trainer t = new Trainer(100, "Ward", "Truyen", LocalDate.of(1984, 9, 3), "Boutersem");
      assertEquals(100, t.getId());
      assertEquals("Ward", t.getFirstName());
      assertEquals("Truyen", t.getLastName());
      assertEquals( LocalDate.of(1984, 9, 3), t.getBirthday());
      assertEquals( "Boutersem", t.getCity());

      assertThrows( IllegalArgumentException.class, ()-> t.setFirstName(null));
      assertThrows( IllegalArgumentException.class, ()-> t.setLastName(null));
      assertThrows( IllegalArgumentException.class, ()-> t.setBirthday(null));
      assertThrows( IllegalArgumentException.class, ()-> t.setCity(null));

      assertThrows( IllegalArgumentException.class, ()-> t.setFirstName(" "));
      assertThrows( IllegalArgumentException.class, ()-> t.setLastName(" "));
      assertThrows( IllegalArgumentException.class, ()-> t.setCity(" "));
   }
}