package be.multimedi.pokemonDB.pokemonPets;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static be.multimedi.pokemonDB.db.DriverManagerWrapper.*;

public final class PokemonPetDAO {
   private PokemonPetDAO() {
   }

   public static List<PokemonPet> getAllPokemonPets() {
      List<PokemonPet> list = new ArrayList<>();
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           Statement stmt = con.createStatement();
           ResultSet rs = stmt.executeQuery("SELECT * FROM PokemonPet");) {
         while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            int speciesId = rs.getInt(3);
            int trainerId = rs.getInt(4);
            try {
               PokemonPet pp = new PokemonPet(id, name, speciesId, trainerId);
               list.add(pp);
            } catch (IllegalArgumentException iae) {
               System.out.println("Could not read PokemonPet: Illegal data in DB! " + iae);
            }
         }//--> while(rs.next())
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return list;
   }

   public static boolean removePokemonPetById(int petId) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           PreparedStatement stmt = con.prepareStatement("DELETE FROM PokemonPet WHERE id = ?");
      ) {
         stmt.setInt(1, petId);
         int changes = stmt.executeUpdate();
         if (changes > 0) {
            return true;
         }
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return false;
   }
}
