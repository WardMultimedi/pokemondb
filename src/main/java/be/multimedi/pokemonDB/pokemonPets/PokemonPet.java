package be.multimedi.pokemonDB.pokemonPets;

public class PokemonPet {
    int id;
    String name;
    int speciesID;
    int trainerID;

    public PokemonPet(int id, String name, int speciesID, int trainerID) {
        setId(id);
        setName(name);
        setSpeciesID(speciesID);
        setTrainerID(trainerID);
    }

    public PokemonPet(String name, int speciesID, int trainerID) {
        this(-1, name, speciesID, trainerID);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeciesID() {
        return speciesID;
    }

    public void setSpeciesID(int speciesID) {
        this.speciesID = speciesID;
    }

    public int getTrainerID() {
        return trainerID;
    }

    public void setTrainerID(int trainerID) {
        this.trainerID = trainerID;
    }

    @Override
    public String toString() {
        return "PokemonPet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", speciesID=" + speciesID +
                ", trainerID=" + trainerID +
                '}';
    }
}