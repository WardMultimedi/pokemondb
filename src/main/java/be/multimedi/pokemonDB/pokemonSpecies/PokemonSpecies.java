package be.multimedi.pokemonDB.pokemonSpecies;

public class PokemonSpecies {
   int id;
   String name;
   int typeId1;
   int typeId2;
   int parentEvolutionId;
   int officialPokemonId;

   public PokemonSpecies(int id, String name, int typeId1, int typeId2, int parentEvolutionId, int officialPokemonId) {
      setId(id);
      setName(name);
      setTypeId1(typeId1);
      setTypeId2(typeId2);
      setParentEvolutionId(parentEvolutionId);
      setOfficialPokemonId(officialPokemonId);
   }

   public PokemonSpecies(String name, int typeId1, int typeId2, int parentEvolutionId, int officialPokemonId) {
      this(-1, name, typeId1, typeId2, parentEvolutionId, officialPokemonId);
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      if (name == null || name.isBlank()) throw new IllegalArgumentException("Name required!");
      this.name = name;
   }

   public int getTypeId1() {
      return typeId1;
   }

   public void setTypeId1(int typeId1) {
      this.typeId1 = typeId1;
   }

   public int getTypeId2() {
      return typeId2;
   }

   public void setTypeId2(int typeId2) {
      this.typeId2 = typeId2;
   }

   public int getParentEvolutionId() {
      return parentEvolutionId;
   }

   public void setParentEvolutionId(int parentEvolutionId) {
      this.parentEvolutionId = parentEvolutionId;
   }

   public int getOfficialPokemonId() {
      return officialPokemonId;
   }

   public void setOfficialPokemonId(int officialPokemonId) {
      this.officialPokemonId = officialPokemonId;
   }

   @Override
   public String toString() {
      return "PokemonSpecies{" +
              "id=" + id +
              ", name='" + name + '\'' +
              ", typeId1=" + typeId1 +
              ", typeId2=" + typeId2 +
              ", parentEvolutionId=" + parentEvolutionId +
              ", officialPokemonId=" + officialPokemonId +
              '}';
   }
}
