package be.multimedi.pokemonDB.trainer;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static be.multimedi.pokemonDB.db.DriverManagerWrapper.*;

public final class TrainerDAO {
   private TrainerDAO() {
   }

   public static Trainer getTrainerById(int id) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           Statement stmt = con.createStatement();
           ResultSet rs = stmt.executeQuery(
                   "SELECT * FROM PokemonTrainer WHERE id = " + id)) {

         if (rs.next()) {
            int trainerId = rs.getInt(1);
            String firstName = rs.getString(2);
            String lastName = rs.getString(3);
            LocalDate birthday = rs.getDate(4).toLocalDate();
            String city = rs.getString(5);
            return new Trainer(trainerId, firstName, lastName, birthday, city);
         }

      } catch (SQLException se) {
         System.out.println("oops : " + se);
      } catch (IllegalArgumentException iae) {
         System.out.println("Illegal data in Db! " + iae);
      }
      return null;
   }

   public static List<Trainer> getTrainersByFirstName(String name) {
      List<Trainer> list = new ArrayList<>();
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           Statement stmt = con.createStatement();
           ResultSet rs = stmt.executeQuery("SELECT * FROM PokemonTrainer WHERE firstName = '" + name + "'");) {
         while (rs.next()) {
            int trainerId = rs.getInt(1);
            String firstName = rs.getString(2);
            String lastName = rs.getString(3);
            LocalDate birthday = rs.getDate(4).toLocalDate();
            String city = rs.getString(5);
            try {
               Trainer t = new Trainer(trainerId, firstName, lastName, birthday, city);
               list.add(t);
            } catch (IllegalArgumentException iae) {
               System.out.println("Could not read Trainer: Illegal data in DB! " + iae);
            }
         }//--> while(rs.next())
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return list;
   }

   public static List<Trainer> getAllTrainers() {
      List<Trainer> list = new ArrayList<>();
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           Statement stmt = con.createStatement();
           ResultSet rs = stmt.executeQuery("SELECT * FROM PokemonTrainer");) {
         while (rs.next()) {
            int trainerId = rs.getInt(1);
            String firstName = rs.getString(2);
            String lastName = rs.getString(3);
            LocalDate birthday = rs.getDate(4).toLocalDate();
            String city = rs.getString(5);
            try {
               Trainer t = new Trainer(trainerId, firstName, lastName, birthday, city);
               list.add(t);
            } catch (IllegalArgumentException iae) {
               System.out.println("Could not read Trainer: Illegal data in DB! " + iae);
            }
         }//--> while(rs.next())
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return list;
   }

   public static boolean registerTrainer(Trainer trainer) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           PreparedStatement stmt = con.prepareStatement("INSERT INTO PokemonTrainer (firstName, lastName, birthday, city) VALUES (?,?,?,?)");
      ) {
         stmt.setString(1, trainer.getFirstName());
         stmt.setString(2, trainer.getLastName());
         stmt.setDate(3, Date.valueOf(trainer.getBirthday()));
         stmt.setString(4, trainer.getCity());
         int changes = stmt.executeUpdate();
         if (changes > 0) {
            return true;
         }
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return false;
   }

   public static boolean removeTrainerById(int trainerID) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           PreparedStatement stmt = con.prepareStatement("DELETE FROM PokemonTrainer WHERE id = ?");
      ) {
         stmt.setInt(1, trainerID);
         int changes = stmt.executeUpdate();
         if (changes > 0) {
            return true;
         }
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return false;
   }

   public static boolean removeTrainer(Trainer trainer) {
      if (trainer == null) return false;
      return removeTrainerById(trainer.getId());
   }

   public static String getTrainerNameById(int trainerID) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           Statement stmt = con.createStatement();
           ResultSet rs = stmt.executeQuery(
                   "SELECT name FROM PokemonTrainer WHERE id = " + trainerID)) {
         if (rs.next()) {
            return rs.getString(1);
         }
      } catch (SQLException se) {
         System.out.println("oops : " + se);
      } catch (IllegalArgumentException iae) {
         System.out.println("Illegal data in Db! " + iae);
      }
      return null;
   }
}
