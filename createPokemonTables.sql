create table PokemonPet
(
    id        int auto_increment
        primary key,
    name      varchar(45) null,
    speciesId int         null,
    trainerId int         null
);

create table PokemonSpecies
(
    id                int auto_increment
        primary key,
    name              varchar(45) not null,
    typeId1           int         not null,
    typeId2           int         null,
    parentEvolutionId int         null,
    officialPokemonId int         null
);

create table PokemonTrainer
(
    id        int auto_increment
        primary key,
    firstName varchar(45) not null,
    lastName  varchar(45) not null,
    birthday  date        not null,
    city      varchar(45) not null
);

create table PokemonType
(
    id   int auto_increment
        primary key,
    type varchar(45) not null
);
